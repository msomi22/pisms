
1) Install MySQL 5.6 and above or MariaDB 10 and above on your local machine.
   Install both client and server applications.



# pip install MySQL-python

# pip install mysql-connector-python

# pip install pymysql

WIN
# pip install pyserial



Database name: smsdb
Database username: sms
Database password: sms12345



2) Create the database user account as follows:

        % mysql -h localhost -u root -p
        Enter password: ******
        mysql> CREATE USER 'sms'@'localhost' IDENTIFIED BY 'sms12345';
        mysql> GRANT ALL ON smsdb.* TO 'sms'@'localhost';
        Query OK, 0 rows affected (0.09 sec)
        mysql> quit
        Bye

    In the commands shown, the % represents the prompt displayed by your shell or
    command interpreter, and mysql> is the prompt displayed by mysql.


3) Create the database as follows:
        % mysql -h localhost -u sms -p
        Enter password: ******
        mysql> CREATE DATABASE smsdb;
        Query OK, 0 rows affected (0.09 sec)
        mysql> quit
        Bye




4) Execute the SQL and data import script as follows:
        % mysql -h localhost -u sms -p smsdb < tables.mysql.sql


