ó
¤}Yc           @   sÏ   d  d l  Z  d  d l Z d  d l Z d Z e d  Z e sQ d GHe j d  n e Z d e GHd d d     YZ d	   Z d
   Z	 d GHd GHe	   Z
 e j d  d GHe e
  e j d  d GHd GHd S(   iÿÿÿÿNs   /dev/ttyACM0s7   Enter Serial Path .e.g "/dev/ttyACM0" without quotes : s9   Path Can't be Empty, default '/dev/ttyACM0' will be used.i   s   Device path is : t   TextMessagec           B   sD   e  Z d  d d  Z d   Z d   Z d   Z d   Z d   Z RS(   s   +254718953974s   TextMessage.content not set.c         C   s   | |  _  | |  _ d  S(   N(   t	   recipientt   content(   t   selfR   t   message(    (    s   sms.pyt   __init__!   s    	c         C   s   | |  _  d  S(   N(   R   (   R   t   number(    (    s   sms.pyt   setRecipient%   s    c         C   s   | |  _  d  S(   N(   R   (   R   R   (    (    s   sms.pyt
   setContent(   s    c         C   sS   t  j t d d d d t d t d t  j d t  j d t  j |  _ t j	 d	  d  S(
   Ni  t   timeouti   t   xonxofft   rtsctst   bytesizet   parityt   stopbitsi   (
   t   serialt   Serialt
   modem_patht   Falset	   EIGHTBITSt   PARITY_NONEt   STOPBITS_ONEt   sert   timet   sleep(   R   (    (    s   sms.pyt   connectPhone+   s    Bc         C   s­   |  j  j d  t j d  |  j  j d  t j d  |  j  j d |  j d  t j d  |  j  j |  j d  t j d  |  j  j t d   t j d  d  S(   Ns   ATZi   s
   AT+CMGF=1s	   AT+CMGS="s   "s   i   (   R   t   writeR   R   R   R   t   chr(   R   (    (    s   sms.pyt   sendMessage/   s    c         C   s   |  j  j   d  S(   N(   R   t   close(   R   (    (    s   sms.pyt   disconnectPhone;   s    (   t   __name__t
   __module__R   R   R   R   R   R   (    (    (    s   sms.pyR        s   				c         C   sæ   t  j t d   } zÉ xÂ | D]º } t |  j d d  } | j d d  } | j d d  } d | } yS t | |   } | j   | j   | j   d |  d | d	 GHt	 j
 d
  Wq t k
 rØ } d GHq Xq WWd  Xd  S(   Ns	   phone.csvt   [t    t   't   ]s   +254s
   message ( s    ) was sent to ( s    ) successfullyi   s   Unable to send message!(   t   csvt   readert   filet   strt   replaceR    R   R   R   R   R   t	   Exception(   t   msgt   csv_datat   rowt   mobilet   smst   e(    (    s   sms.pyt   importFromCSV?   s"    



c          C   sP   y1 t  d d  }  |  j   } | s, d GHn  | SWn t k
 rK } d GHn Xd  S(   Ns   message.txtt   rs   No message found!s!   File not found! or can't be read!(   t   opent   readR*   (   R'   R   R0   (    (    s   sms.pyt   readMessageFromFileY   s    sH   ************************************************************************s   Reading message fromtext files#   Importing contacts from csv file...s    Done sending text messages, bye.(    (   R   R   R%   R   t	   raw_inputt   pathR   R    R1   R5   t   messageToSend(    (    (    s   sms.pyt   <module>   s*   				
