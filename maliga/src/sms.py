#!/usr/bin/env python2
# coding: utf-8


'''

**** NOTE ****

Please do not modify anything if you are not sure , else the program may no work.


'''

import serial
import time
import csv

modem_path = "/dev/ttyACM0" 
path = raw_input('Enter Serial Path .e.g "/dev/ttyACM0" without quotes : ')

if not path:
    print "Path Can't be Empty, default '/dev/ttyACM0' will be used."
    time.sleep(1)
    

else:
    modem_path = path 
    pass

print "Device path is : " + modem_path

class TextMessage:
    def __init__(self, recipient="+254718953974", message="TextMessage.content not set."):
        self.recipient = recipient
        self.content = message

    def setRecipient(self, number):
        self.recipient = number

    def setContent(self, message):
        self.content = message

    def connectPhone(self):
        self.ser = serial.Serial(modem_path, 460800, timeout=5, xonxoff = False, rtscts = False, bytesize = serial.EIGHTBITS, parity = serial.PARITY_NONE, stopbits = serial.STOPBITS_ONE)
        time.sleep(1)

    def sendMessage(self):
        self.ser.write('ATZ\r')
        time.sleep(1)
        self.ser.write('AT+CMGF=1\r')
        time.sleep(1)
        self.ser.write('''AT+CMGS="''' + self.recipient + '''"\r''')
        time.sleep(1)
        self.ser.write(self.content + "\r")
        time.sleep(1)
        self.ser.write(chr(26))
        time.sleep(1)

    def disconnectPhone(self):
        self.ser.close()


def importFromCSV(msg):
    csv_data = csv.reader(file('phone.csv')) 
    try:
        for row in csv_data:
            mobile = str(row).replace("[",'')
            mobile = mobile.replace("'",'')
            mobile = mobile.replace("]",'')
            mobile = '+254'+mobile 
            #print "mobile:" + mobile + " , message: " + msg 
            try:
                pass
                sms = TextMessage(mobile,msg)
                sms.connectPhone()
                sms.sendMessage()
                sms.disconnectPhone()
                print "message ( " + msg +" ) was sent to ( " + mobile + " ) successfully"
                time.sleep(1)#sleep to  avoid creating congestion in the network 

            except Exception as e:
                print "Unable to send message!"
            
       
    finally:
        pass


def readMessageFromFile():
    try:
        file = open("message.txt", "r") 
        message = file.read()
        if not message:
            print "No message found!"
        return message
     
    except Exception as e:
        print "File not found! or can't be read!"      
       
        

print "************************************************************************"
print "Reading message fromtext file"
messageToSend = readMessageFromFile()  
time.sleep(1) 
print "Importing contacts from csv file..." 
importFromCSV(messageToSend)
time.sleep(1)
print "Done sending text messages, bye."   
print "************************************************************************"


