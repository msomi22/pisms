#!/usr/bin/env python2
# coding: utf-8

import serial
import time
import pymysql
import csv
import uuid

modem_path = "/dev/ttyUSB_utps_modem" 
path = raw_input("Enter Serial Path:")

if not path:
    print "Path Can't be Empty, default '/dev/ttyACM0' will be used."
    time.sleep(1)

else:
    modem_path = path 
    pass

print "Device path is : " + modem_path

class TextMessage:
    def __init__(self, recipient="+254718953974", message="TextMessage.content not set."):
        self.recipient = recipient
        self.content = message

    def setRecipient(self, number):
        self.recipient = number

    def setContent(self, message):
        self.content = message

    def connectPhone(self):
        self.ser = serial.Serial(modem_path, 460800, timeout=5, xonxoff = False, rtscts = False, bytesize = serial.EIGHTBITS, parity = serial.PARITY_NONE, stopbits = serial.STOPBITS_ONE)
        time.sleep(1)

    def sendMessage(self):
        self.ser.write('ATZ\r')
        time.sleep(1)
        self.ser.write('AT+CMGF=1\r')
        time.sleep(1)
        self.ser.write('''AT+CMGS="''' + self.recipient + '''"\r''')
        time.sleep(1)
        self.ser.write(self.content + "\r")
        time.sleep(1)
        self.ser.write(chr(26))
        time.sleep(1)

    def disconnectPhone(self):
        self.ser.close()



def doQuery(conn):
    cur = conn.cursor()
    cur.execute("SELECT mobile FROM contact")
    message = "Hello , message sent from computer"
    phone = ""
    for mobile in cur.fetchall():
        mobilestr = str(mobile).replace("(",'')
        mobilestr = mobilestr.replace("'",'')
        mobilestr = mobilestr.replace(")",'')
        mobilestr = mobilestr.replace(",",'')
        phone = '+254'+mobilestr 
        #print phone
        sms = TextMessage(phone,message) 
        sms.connectPhone()
        sms.sendMessage()
        sms.disconnectPhone()
        print "message was sent to ( " + phone + " ) successfully"
        time.sleep(1)#sleep to  avoid creating congestion in the network 


def deleteExistingContact(conn):
    try:
        sql = "DELETE FROM `contact`"
        with conn.cursor() as cursor:
            cursor.execute(sql)
            conn.commit()
        
    finally:
        pass



def importFromCSV(conn):
    csv_data = csv.reader(file('phone.csv')) 
    try:
        for row in csv_data:
             with conn.cursor() as cursor:
                # Create a new record
                mobile = str(row).replace("[",'')
                mobile = mobile.replace("'",'')
                mobile = mobile.replace("]",'') 
                #print "uuid: " + str(uuid.uuid4()) + ", mobile:" + mobile
                sql = "INSERT INTO `contact` (`uuid`,`mobile`) VALUES (%s, %s)"
                cursor.execute(sql,(str(uuid.uuid4()), mobile ))
                # connection is not autocommit by default. 
                # So you must commit to save
                # your changes.
                conn.commit()
       
    finally:
        pass
        #conn.close()
        

hostname = 'localhost'
username = 'sms'
password = 'sms12345'
database = 'smsdb'

myConnection = pymysql.connect( host=hostname, user=username, passwd=password, db=database )
print "************************************************************************"
print "Importing contacts to database Using pymysql..."
deleteExistingContact(myConnection)
time.sleep(1)
importFromCSV(myConnection)  
print "Done Importing contacts..."
print "************************************************************************"
time.sleep(1)
print "Get contact from database Using pymysql..."
doQuery(myConnection)
myConnection.close()  
print "************************************************************************"   
print "Done sending text messages, bye."   
print "************************************************************************"


