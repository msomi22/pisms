


DROP TABLE IF EXISTS contact;

-- -------------------
-- Table contact
-- -------------------
CREATE TABLE contact (
    uuid VARCHAR(100) UNIQUE NOT NULL,
    mobile VARCHAR(20),
    dateAdded TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (uuid)
) ENGINE=InnoDB;

LOAD DATA LOCAL INFILE 'sms.csv' INTO TABLE contact FIELDS TERMINATED BY '|' IGNORE 1 LINES;

